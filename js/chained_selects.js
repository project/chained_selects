(function ($) {

/**
 * Chain select lists.
 */
Drupal.behaviors.chainedSelects = {
  attach: function (context) {
    var chainedSelects = Drupal.settings.chainedSelects;
    for (var key in chainedSelects) {
      $('#' + chainedSelects[key]['child'] + ':input', context).once(function() {
        $(this).chained('#' + chainedSelects[key]['parent'] + ':input');
      })
    }
  }
};

})(jQuery);
